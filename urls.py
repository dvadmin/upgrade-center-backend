# -*- coding: utf-8 -*-
from django.urls import re_path, path
from rest_framework import routers
from plugins.upgrade_center_backend.views.app_manage import ApplicationViewSet
from plugins.upgrade_center_backend.views.app_version import AppVersionViewSet, UpdateRecordViewSet
from plugins.upgrade_center_backend.views.version_request import VersionRequestViewSet

router_url = routers.SimpleRouter()
router_url.register(r'application', ApplicationViewSet)
router_url.register(r'appversion', AppVersionViewSet)


urlpatterns = [
    path('update/',UpdateRecordViewSet.as_view({'post':'update_record'}),),
    path('version_request/',VersionRequestViewSet.as_view({'get':'list'}),)
]
urlpatterns += router_url.urls

