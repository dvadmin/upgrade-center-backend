# -*- coding: utf-8 -*-

"""
@author: 猿小天
@contact: QQ:1638245306
@Created on: 2021/11/20 020 10:34
@Remark:
"""
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from plugins.upgrade_center_backend.models import VersionRequest


class VersionRequestSerializer(CustomModelSerializer):
    """
    版本访问管理-序列化器
    """
    class Meta:
        model = VersionRequest
        fields = "__all__"
        read_only_fields = ["id"]


class VersionRequestViewSet(CustomModelViewSet):
    """
    APP版本管理接口
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = VersionRequest.objects.all()
    serializer_class = VersionRequestSerializer