# -*- coding: utf-8 -*-

"""
@author: 猿小天
@contact: QQ:1638245306
@Created on: 2021/11/19 019 20:59
@Remark:
"""
from dvadmin.utils.serializers import CustomModelSerializer
from dvadmin.utils.viewset import CustomModelViewSet
from plugins.upgrade_center_backend.models import Application


class ApplicationSerializer(CustomModelSerializer):
    """
    app管理-序列化器
    """

    class Meta:
        model = Application
        fields = "__all__"
        read_only_fields = ["id"]


class ApplicationViewSet(CustomModelViewSet):
    """
    升级中心APP管理接口
    list:查询
    create:新增
    update:修改
    retrieve:单例
    destroy:删除
    """
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
