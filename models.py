# -*- coding: utf-8 -*-

"""
@author: 猿小天
@contact: QQ:1638245306
@Created on: 2021/11/19   19:54
@Remark:
"""

from django.db import models
from django.db.models import CASCADE

from dvadmin.system.models import FileList
from dvadmin.utils.models import CoreModel


class Application(CoreModel):
    name = models.CharField(max_length=50,help_text="软件名称")
    app_id = models.CharField(max_length=100,unique=True,help_text="APP标识")

    class Meta:
        db_table = "upgarde_center_application"
        verbose_name = "app管理表"
        verbose_name_plural = verbose_name


class AppVersion(CoreModel):
    application = models.ForeignKey(Application,db_constraint=False,on_delete=models.PROTECT,help_text="APP管理")
    app_id =  models.CharField(max_length=100,help_text="APP标识")
    type_choices = (
        (0,"整包"),
        (1,"部分")
    )
    upgrade_type = models.IntegerField(choices=type_choices,default=0,help_text="升级类型")
    title = models.CharField(max_length=50,help_text="标题")
    content = models.CharField(max_length=1000,help_text="内容")
    platform_choices = (
        (0,"windows"),
        (1,"linux")
    )
    platform = models.IntegerField(choices=platform_choices,default=0,help_text="平台")
    version = models.CharField(max_length=20,help_text="版本号")
    file_url = models.ForeignKey(FileList, on_delete=CASCADE, verbose_name="关联文件地址", db_constraint=False, help_text="关联文件地址")
    coerce_upgrade = models.BooleanField(default=False,help_text="是否强制升级")
    status = models.BooleanField(default=True,help_text="是否上线发行")

    class Meta:
        db_table = "upgarde_center_appversion"
        verbose_name = "app版本表"
        verbose_name_plural = verbose_name
        ordering = ['-create_datetime']


class VersionRequest(CoreModel):
    app_id = models.CharField(max_length=100, help_text="APP标识")
    device = models.CharField(max_length=255,null=True,blank=True,help_text="设备")
    version = models.CharField(max_length=20,help_text="版本号")
    platform = models.IntegerField(help_text="平台")

    class Meta:
        db_table = "upgarde_center_versionrequest"
        verbose_name = "app版本请求表"
        verbose_name_plural = verbose_name
