# 初始化
import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'application.settings')
django.setup()
from dvadmin.utils.core_initialize import CoreInitialize

from dvadmin.system.models import Menu, MenuButton


class Initialize(CoreInitialize):
    creator_id = "456b688c-8ad5-46de-bc2e-d41d8047bd42"

    def init_menu(self):
        """
        初始化菜单表
        """
        self.menu_data = [
            {"id": 28, "name": "APP升级中心", "sort": 4, "web_path": "",
             "icon": "upload", "parent_id": None},
            {"id": 29, "name": "APP管理", "sort": 1, "web_path": "/application",
             "icon": "apple", "parent_id": 28,
             "component": "dvadmin_plugins/upgrade_center_web/application/index",
             "component_name": "application"},
            {"id": 30, "name": "app版本管理", "sort": 2, "web_path": "/appversion/:id",
             "icon": "television", "parent_id": 28, "visible": 0,
             "component": "dvadmin_plugins/upgrade_center_web/version/index",
             "component_name": "appversion"},
            {"id": 31, "name": "更新记录", "sort": 3, "web_path": "/updateRecord",
             "icon": "sliders", "parent_id": 28,
             "component": "dvadmin_plugins/upgrade_center_web/update_record/index",
             "component_name": "updateRecord"},
        ]
        self.save(Menu, self.menu_data, "菜单表")

    def init_menu_button(self):
        """
        初始化菜单权限表
        """
        self.menu_button_data = [
            {'id': 88, 'menu_id': 29,
             'name': '查询', 'value': 'Search', 'api': '/api/upgrade_center_backend/application/',
             'method': 0},
            {'id': 89, 'menu_id': 29,
             'name': '新增', 'value': 'Create', 'api': '/api/upgrade_center_backend/application/',
             'method': 1},
            {'id': 90, 'menu_id': 29,
             'name': '编辑', 'value': 'Update', 'api': '/api/upgrade_center_backend/application/{id}/',
             'method': 2},
            {'id': 91, 'menu_id': 29,
             'name': '删除', 'value': 'Delete', 'api': '/api/upgrade_center_backend/application/{id}/',
             'method': 3},
            {'id': 92, 'menu_id': 29,
             'name': '单例', 'value': 'Retrieve',
             'api': '/api/upgrade_center_backend/application/{id}/', 'method': 0},
            {'id': 93, 'menu_id': 30,
             'name': '查询', 'value': 'Search', 'api': '/api/upgrade_center_backend/version/',
             'method': 0},
            {'id': 94, 'menu_id': 30,
             'name': '新增', 'value': 'Create', 'api': '/api/upgrade_center_backend/version/',
             'method': 1},
            {'id': 95, 'menu_id': 30,
             'name': '编辑', 'value': 'Update', 'api': '/api/upgrade_center_backend/version/{id}/',
             'method': 2},
            {'id': 96, 'menu_id': 30,
             'name': '删除', 'value': 'Delete', 'api': '/api/upgrade_center_backend/version/{id}/',
             'method': 3},
            {'id': 97, 'menu_id': 30,
             'name': '单例', 'value': 'Retrieve', 'api': '/api/upgrade_center_backend/version/{id}/',
             'method': 0},
            {'id': 98, 'menu_id': 31,
             'name': '查询', 'value': 'Search', 'api': '/api/upgrade_center_backend/version_request/',
             'method': 0},
            {'id': 99, 'menu_id': 31,
             'name': '新增', 'value': 'Create', 'api': '/api/upgrade_center_backend/version_request/',
             'method': 1},
            {'id': 100, 'menu_id': 31,
             'name': '编辑', 'value': 'Update',
             'api': '/api/upgrade_center_backend/version_request/{id}/', 'method': 2},
            {'id': 101, 'menu_id': 31,
             'name': '删除', 'value': 'Delete',
             'api': '/api/upgrade_center_backend/version_request/{id}/', 'method': 3},
            {'id': 102, 'menu_id': 31,
             'name': '单例', 'value': 'Retrieve',
             'api': '/api/upgrade_center_backend/version_request/{id}/', 'method': 0}]
        self.save(MenuButton, self.menu_button_data, "菜单权限表")

    def run(self):
        self.init_menu()
        self.init_menu_button()


# 项目init 初始化，默认会执行 main 方法进行初始化
def main(reset=False):
    Initialize(reset).run()
    pass


if __name__ == '__main__':
    main()
