from django.apps import AppConfig


class UpgradeCenterBackendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'plugins.upgrade_center_backend'
    url_prefix = "upgrade_center_backend"
